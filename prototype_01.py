import time
import hashlib
from flask import Flask, render_template, request, redirect, url_for, jsonify, session, escape, Response
from pymongo import MongoClient
import json
from random import randint
from bson.objectid import ObjectId
import string
import xlwt
import os
import smtplib
# For guessing MIME type
import mimetypes
# Import the email modules we'll need
import email
import email.mime.application

alphabets = string.ascii_lowercase


def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)


client = MongoClient()
db = client.s_prototype_01
col_user_credentials = db.user_credentials
# user_credential = {
#     'username':username,
#     'password':password.encode('base64','strict'),
#     'company_name':company_name,
#     'address':address,
#     'name':name,
#     'm_num':m_num,
#     'l_num':l_num,
#     'email':email,
#     'gstin':gstin,
#     'pan':pan,
#     # 'category':category,
#     'update_category':update_category
# }
col_stories = db.stories 
# story = {
# id
# story_name
# story_des
# story_image
# visibility
# }
col_fabrics = db.fabric_data
# {
#     'fabric_id':
#     'fabric_name':
#     'fabric_desc':
#     'fabric_image':
#     'fabric_price:
# }
col_designs = db.design_data
# design = {'design_name':d_name, 'design_id':p_id, 'design_fabrics': d_fabrics, 'design_stories':d_stories, 'colors':[]}
col_user_orders = db.user_orders

def check_auth(username, password): #This function is called to check if a username /password combination is valid.        
    user_credential = col_user_credentials.find_one({'username':username})
    if user_credential:        
        if password.encode('base64','strict') == user_credential['password']:
            return user_credential
        else:
            return False
    else:
        return False

app = Flask(__name__)

@app.route('/')
def home():
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])
        if user_credential:
            return redirect(url_for('displaystories', username=session['username']))
    else:
        return render_template('home.html', data={'auth':'true'})

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])
        if user_credential:
            return redirect(url_for('displaystories', username=session['username']))
    else:
        return render_template('signup.html', data='data')

@app.route('/validate', methods=['GET', 'POST'])
def validate():
    if (request.method == 'POST'):
        username = request.form['username']
        valid = 'false'
        if not col_user_credentials.find_one({'username':username}):
            valid = 'true'
        return valid

@app.route('/signin_up', methods=['GET', 'POST'])
def signin_up():
    if (request.method == 'POST'):        
        if request.form['func'] == 'login':
            username = request.form['username']
            password = request.form['pwd']
            if check_auth(username,password):
                session['username'] = username
                session['password'] = password
                return redirect(url_for('displaystories', username=session['username']))
            else:
                return render_template('home.html', data={'auth':'false'})
        else:
            username = request.form['username'].strip()
            password = request.form['pwd'].strip()
            company_name = request.form['company_name'].strip()
            address_details = request.form['address_text'].strip()
            state = request.form['state'].strip()
            city = request.form['city'].strip()
            pin_code = request.form['pin_code'].strip()
            name = request.form['name'].strip()
            m_num = request.form['m_num'].strip()
            l_num = request.form['l_num'].strip()
            email = request.form['email'].strip()
            gstin = request.form['gstin'].strip()
            pan = request.form['pan'].strip()
            # category = request.form['category']
            # update_category = request.form['update_category']

            address = {'address_details':address_details,'state':state, 'city':city, 'pin_code':pin_code}
            if not col_user_credentials.find_one({'username':username}):
                user_credential = {
                    'username':username,
                    'password':password.encode('base64','strict'),
                    'company_name':company_name,
                    'address':address,
                    'name':name,
                    'm_num':m_num,
                    'l_num':l_num,
                    'email':email,
                    'gstin':gstin,
                    'pan':pan,
                    # 'category':category,
                    'update_category':'Basic'
                }                                
                col_user_credentials.insert_one(user_credential)
                session['username'] = username
                session['password'] = password
                return redirect(url_for('displaystories', username=username))

@app.route('/logout')
def logout():    
    session.pop('username', None)
    session.pop('password', None)    
    return redirect(url_for('home'))

####################################################################################
# Admin Operations

@app.route('/uploadstory', methods=['GET', 'POST'])
def uploadstory():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                s_name = request.form['s_name']
                s_desc = request.form['s_desc']
                s_uuid = request.form['s_uuid']
                visibility = request.form['visibility']
                story = {
                        'story_id':random_with_N_digits(5),
                        'story_name':s_name,
                        'story_desc':s_desc,
                        'story_image':s_uuid,
                        'visibility':visibility,
                        }                
                col_stories.insert_one(story)
                return redirect(url_for('viewallstories'))
            else:
                return render_template('upload_story.html', data='')

@app.route('/viewallstories', methods=['GET', 'POST'])
def viewallstories():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                story_image = request.form['s_uuid']
                col_stories.delete_one({'story_image':story_image})

                for design in col_designs.find():
                    if story_image in design['design_stories']:
                        _id = design['_id']
                        col_designs.update( {"_id":_id}, { "$pull": {'design_stories' : story_image} })

                return redirect(url_for('viewallstories'))
            else:
                all_stories = []
                for story_doc in col_stories.find():
                    all_stories.append(story_doc)
                return render_template('view_all_stories.html', data={'all_stories':all_stories})

@app.route('/editstory/<story_image>', methods=['GET', 'POST'])
def editstory(story_image):
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):                
                s_name = request.form['s_name']
                s_desc = request.form['s_desc']                
                if request.form['visibility']:
                    visibility = request.form['visibility']
                else:
                    visibility = request.form['c_vis']
                col_stories.update(
                    {'story_image':story_image},
                    {'$set':
                        {
                        'story_name':s_name,
                        'story_desc':s_desc,
                        'visibility':visibility,
                        }
                    }
                )
                return redirect(url_for('viewallstories'))
            else:
                story_data = col_stories.find_one({'story_image':story_image})
                return render_template('edit_story.html', data={'story_data':story_data})

@app.route('/uploadfabric', methods=['GET', 'POST'])
def uploadfabric():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                f_name = request.form['f_name']
                f_desc = request.form['f_desc']
                f_uuid = request.form['f_uuid']
                f_price = {
                    'swatch':request.form['f_s'],
                    '5-20':request.form['f_5_20'],
                    '20-50':request.form['f_20_50'],
                    '50-100':request.form['f_50_100'],
                    '100-':request.form['f_100_'],
                }
                fabric = {
                        'fabric_id':random_with_N_digits(3),
                        'fabric_name':f_name,
                        'fabric_desc':f_desc,
                        'fabric_image':f_uuid,
                        'fabric_price':f_price,
                        }                
                col_fabrics.insert_one(fabric)
                return redirect(url_for('viewallfabrics'))
            else:
                return render_template('upload_fabric.html', data='')

@app.route('/viewallfabrics', methods=['GET', 'POST'])
def viewallfabrics():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                
                fabric_image = request.form['s_uuid']
                col_fabrics.delete_one({'fabric_image':fabric_image})

                for design in col_designs.find():
                    if fabric_image in design['design_fabrics']:
                        _id = design['_id']
                        col_designs.update( {"_id":_id}, { "$pull": {'design_fabrics' : fabric_image} })

                return redirect(url_for('viewallfabrics'))
            else:
                all_fabrics = []
                for fabric_doc in col_fabrics.find():
                    all_fabrics.append(fabric_doc)
                return render_template('view_all_fabrics.html', data={'all_fabrics':all_fabrics})

@app.route('/editfabric/<fabric_image>', methods=['GET', 'POST'])
def editfabric(fabric_image):
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):                
                f_name = request.form['f_name']
                f_desc = request.form['f_desc']
                f_uuid = request.form['f_uuid']
                f_price = {
                    'swatch':request.form['f_s'],
                    '5-20':request.form['f_5_20'],
                    '20-50':request.form['f_20_50'],
                    '50-100':request.form['f_50_100'],
                    '100-':request.form['f_100_'],
                }                
                col_fabrics.update(
                    {'fabric_image':fabric_image},
                    {'$set':
                        {
                        'fabric_name':f_name,
                        'fabric_desc':f_desc,
                        'fabric_image':f_uuid,
                        'fabric_price':f_price,
                        }
                    }
                )                
                return redirect(url_for('viewallfabrics'))
            else:
                fabric_data = col_fabrics.find_one({'fabric_image':fabric_image})
                return render_template('edit_fabric.html', data={'fabric_data':fabric_data})


@app.route('/uploaddesign', methods=['GET', 'POST'])
def uploaddesign():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):
            if (request.method == 'POST'):
                d_uid = request.form['d_uid'] #User inputed id
                d_id = random_with_N_digits(8)
                d_fabrics = request.form.getlist('d_fabrics')
                d_stories = request.form.getlist('d_stories')
                design = {'design_id':d_id, 'design_uid':d_uid, 'design_fabrics': d_fabrics, 'design_stories':d_stories,
                    'colors':[]}
                col_designs.insert_one(design)
                return redirect(url_for('uploadcolor', d_id=d_id, d_uid=d_uid))
            else:
                all_fabrics = []
                for fabric_doc in col_fabrics.find():
                    all_fabrics.append(fabric_doc)
                all_stories = []
                for story_doc in col_stories.find():
                    all_stories.append(story_doc)
                return render_template('upload_design.html', data={'all_fabrics':all_fabrics,'all_stories':all_stories})

@app.route('/uploadcolor/<d_id>/<d_uid>', methods=['GET', 'POST'])
def uploadcolor(d_id,d_uid):
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):
            if (request.method == 'POST'):
                c_uuid = request.form['c_uuid']
                c_name = request.form['c_name']                
                color = {'color_name':c_name, 'color_image':c_uuid}
                col_designs.update({"design_id": int(d_id)}, {'$push': {'colors': color}})                
                return redirect(url_for('uploadcolor', d_id=d_id, d_uid=d_uid))
            else:
                return render_template('upload_color.html', data={'d_id':d_id, 'd_uid':d_uid})

@app.route('/viewalldesigns', methods=['GET', 'POST'])
def viewalldesigns():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                d_id = request.form['s_uuid']
                col_designs.delete_one({'design_id':int(d_id)})
                return redirect(url_for('viewalldesigns'))
            else:
                all_designs = []
                for design_doc in col_designs.find():
                    
                    stories = design_doc['design_stories']
                    stories_name = []
                    for story in stories:
                        stories_name.append(col_stories.find_one({'story_image':story})['story_name'])
                    design_doc['stories_name'] = stories_name

                    fabrics = design_doc['design_fabrics']
                    fabrics_name = []
                    for fabric in fabrics:
                        fabrics_name.append(col_fabrics.find_one({'fabric_image':fabric})['fabric_name'])
                    design_doc['fabrics_name'] = fabrics_name
                    
                    all_designs.append(design_doc)
                return render_template('view_all_designs.html', data={'all_designs':all_designs})

@app.route('/editdesign/<design_id>', methods=['GET', 'POST'])
def editdesign(design_id):
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):                
            if (request.method == 'POST'):
                d_uid = request.form['d_uid'] #User inputed id
                d_fabrics = request.form.getlist('d_fabrics')
                d_stories = request.form.getlist('d_stories')
                col_designs.update(
                    {'design_id':int(design_id)},
                    {'$set':
                        {
                        'design_uid':d_uid,
                        'design_fabrics':d_fabrics,
                        'design_stories':d_stories,
                        }
                    }
                )
                return redirect(url_for('viewalldesigns'))
            else:                
                all_fabrics = []
                for fabric_doc in col_fabrics.find():
                    all_fabrics.append(fabric_doc)
                all_stories = []
                for story_doc in col_stories.find():
                    all_stories.append(story_doc)
                design_doc = col_designs.find_one({'design_id':int(design_id)})
                return render_template('edit_design.html', data={'all_fabrics':all_fabrics,'all_stories':all_stories,'design_data':design_doc})

@app.route('/viewallusers', methods=['GET', 'POST'])
def viewallusers():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):            
            if (request.method == 'POST'):
                username = request.form['username']
                update_category = request.form['update_category']                
                col_user_credentials.update(
                    {'username':username},
                    {'$set':
                        {
                        'update_category':update_category
                        }
                    }
                )  
                return 'true'
            else:
                all_users = []
                for user_doc in col_user_credentials.find():
                    all_users.append(user_doc)
                return render_template('view_all_users.html', data={'all_users':all_users})

@app.route('/viewallorders', methods=['GET', 'POST'])
def viewallorders():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):
            if (request.method == 'POST'):
                string_id = request.form['string_id']
                order_update_one = col_user_orders.update(
                    {'_id':ObjectId(string_id)},
                    {'$set':
                        {
                        'order_status':'delivered'
                        }
                    }
                )
                return string_id
            else:
                all_orders = []
                for order_doc in col_user_orders.find():
                    order_doc['color_doc'] = (item for item in order_doc['design_doc']['colors'] if item["color_image"] == order_doc['order_color']).next()

                    order_username = order_doc['order_username']
                    user_doc = col_user_credentials.find_one({'username':order_username})
                    order_doc['user_doc'] = user_doc

                    _id = order_doc['_id']
                    order_doc['string_id'] = str(_id)

                    all_orders.append(order_doc)
                all_orders.reverse()

                return render_template('view_all_orders.html', data={'all_orders':all_orders})

@app.route('/writetoxl', methods=['GET', 'POST'])
def writetoxl():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):
            if (request.method == 'POST'):
                all_orders = []
                for order_doc in col_user_orders.find():

                    story_image = order_doc['order_story']
                    story_doc = col_stories.find_one({'story_image':story_image})
                    order_doc['story_doc'] = story_doc

                    fabric_image = order_doc['order_fabric']
                    fabric_doc = col_fabrics.find_one({'fabric_image':fabric_image})
                    order_doc['fabric_doc'] = fabric_doc

                    design_id = int(order_doc['order_design'])
                    design_doc = col_designs.find_one({'design_id':design_id})                    
                    order_doc['design_doc'] = design_doc

                    for color in design_doc['colors']:
                        if color['color_image'] == order_doc['order_color']:
                            order_doc['color_doc'] = color

                    order_username = order_doc['order_username']
                    user_doc = col_user_credentials.find_one({'username':order_username})
                    order_doc['user_doc'] = user_doc

                    _id = order_doc['_id']
                    order_doc['string_id'] = str(_id)

                    all_orders.append(order_doc)
                all_orders.reverse()

                wb = xlwt.Workbook()
                ws = wb.add_sheet('Orders')
                style0 = xlwt.easyxf('font: name Times New Roman, bold on')
                ws.write(0, 0, 'Username', style0)
                ws.write(0, 1, 'Name', style0)
                ws.write(0, 2, "Company Name", style0)
                ws.write(0, 3, "Story", style0)
                ws.write(0, 4, "Design ID", style0)
                ws.write(0, 5, 'Color ID', style0)
                ws.write(0, 6, 'Fabric', style0)
                ws.write(0, 7, 'Quantity', style0)
                ws.write(0, 8, 'Date', style0)
                ws.write(0, 9, 'Time', style0)
                ws.write(0, 10, 'Status', style0)
                wb.save('/tmp/orders.xls')

                for w_o in range(len(all_orders)):
                    r = w_o                    
                    
                    ws.write(r+1, 0, all_orders[w_o]['order_username'])
                    ws.write(r+1, 1, all_orders[w_o]['user_doc']['name'])
                    ws.write(r+1, 2, all_orders[w_o]['user_doc']['company_name'])
                    ws.write(r+1, 3, all_orders[w_o]['story_doc']['story_name'])
                    ws.write(r+1, 4, all_orders[w_o]['design_doc']['design_id'])
                    if 'color_id' in all_orders[w_o]['color_doc']:
                        ws.write(r+1,5, all_orders[w_o]['color_doc']['color_id'])
                    else:
                        ws.write(r+1, 5, '')
                    ws.write(r+1, 6, all_orders[w_o]['fabric_doc']['fabric_name'])
                    ws.write(r+1, 7, all_orders[w_o]['order_qty'])
                    ws.write(r+1, 8, all_orders[w_o]['order_date'])
                    ws.write(r+1, 9, all_orders[w_o]['order_time'])
                    ws.write(r+1, 10, all_orders[w_o]['order_status'])
                    wb.save('/tmp/orders.xls')

                sender = 'badricloudaws@gmail.com'
                receivers = ['vmbadri92@gmail.com', 'sankalpagarwal294@gmail.com']
                
                msg = email.mime.Multipart.MIMEMultipart()
                msg['Subject'] = 'Orders till '+time.strftime("%d/%m/%Y")+' , '+time.strftime("%H:%M:%S")
                msg['From'] = 'badricloudaws@gmail.com'
                msg['To'] = 'sankalpagarwal294@gmail.com'

                # The main body is just another attachment
                body = email.mime.Text.MIMEText("""ORDERS""")
                msg.attach(body)

                # xls attachment
                filename='/tmp/orders.xls'
                fp=open(filename,'rb')
                att = email.mime.application.MIMEApplication(fp.read(),_subtype="xls")
                fp.close()
                att.add_header('Content-Disposition','attachment',filename=filename)
                msg.attach(att)
                
                s = smtplib.SMTP('in-v3.mailjet.com', 587)
                s.starttls()
                s.login('a2f9101587dc0b7aec3509c0a35b99d8', 'd4d1385b7e7b2892074f49731ed192a5')
                s.sendmail(sender, receivers, msg.as_string())
                s.quit()                
                os.remove('/tmp/orders.xls')
                return 'true'

@app.route('/adminpages')
def adminpages():
    if ('username' in session) and ('password' in session):
        if (session['username']== 'admin') & (session['password']=='admin'):
            return render_template('admin_pages.html', data={})                

# Admin Operations End
####################################################################################


@app.route('/displaystories/<username>')
def displaystories(username):
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])        
        if user_credential:
            if session['username'] == username:                
                v = user_credential['update_category']

                all_stories = []
                for story_doc in col_stories.find():
                    visibility = story_doc['visibility']
                    if visibility == 'Public':
                        all_stories.append(story_doc)
                    elif visibility == v:                        
                        all_stories.append(story_doc)
                    elif visibility == username:
                        all_stories.append(story_doc)
                return render_template('display_stories.html', data={'all_stories':all_stories, 'user':user_credential})

@app.route('/displaydesigns/<story_uuid>')
def displaydesigns(story_uuid):
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])        
        if user_credential:
                        
            v = user_credential['update_category']

            story_data = col_stories.find_one({'story_image':story_uuid})
            vis = story_data['visibility']
            if vis == 'Public' or (vis == v) or (vis == session['username']):                

                all_designs = []                
                for design_doc in col_designs.find():
                    for story in design_doc['design_stories']:
                        if story == story_uuid:
                            stories = design_doc['design_stories']
                            stories_name = []
                            for story in stories:
                                stories_name.append(col_stories.find_one({'story_image':story})['story_name'])
                            design_doc['stories_name'] = stories_name

                            fabrics = design_doc['design_fabrics']
                            fabrics_name = []
                            for fabric in fabrics:
                                fabrics_name.append(col_fabrics.find_one({'fabric_image':fabric})['fabric_name'])
                            design_doc['fabrics_name'] = fabrics_name
                            all_designs.append(design_doc)                
                return render_template('display_designs.html', data={'all_designs':all_designs, 'user':user_credential, 'story_data':story_data})

@app.route('/displaycolors/<story_uuid>/<design_id>')
def displaycolors(story_uuid,design_id):
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])
        if user_credential:            
            v = user_credential['update_category']

            story_data = col_stories.find_one({'story_image':story_uuid})
            vis = story_data['visibility']
            if vis == 'Public' or (vis == v) or (vis == session['username']):                 
                design_doc = col_designs.find_one({'design_id':int(design_id)})
                stories = design_doc['design_stories']
                stories_name = []
                for story in stories:
                    stories_name.append(col_stories.find_one({'story_image':story})['story_name'])
                design_doc['stories_name'] = stories_name

                fabrics = design_doc['design_fabrics']
                fabrics_name = []
                for fabric in fabrics:
                    f = col_fabrics.find_one({'fabric_image':fabric})
                    del f['_id']
                    fabrics_name.append(f)
                design_doc['fabrics_name'] = fabrics_name
                del design_doc['_id']                
                return render_template('display_colors.html', data={'design':design_doc,'design_json':json.dumps(design_doc), 'story_data':story_data, 'user':user_credential})

@app.route('/orders', methods=['GET', 'POST'])
def orders():
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])
        if user_credential:
            if (request.method == 'POST'):
                order_username = request.form['o_username']
                order_story = request.form['o_story']
                order_design = request.form['o_design']
                order_color = request.form['o_color']
                order_fabric = request.form['o_fabric']
                order_qty = request.form['o_qty']

                story_image = order_story
                story_doc = col_stories.find_one({'story_image':story_image})

                fabric_image = order_fabric
                fabric_doc = col_fabrics.find_one({'fabric_image':fabric_image})

                design_id = int(order_design)
                design_doc = col_designs.find_one({'design_id':design_id})

                order = {
                    'order_username':order_username,
                    'order_story':order_story,
                    'story_doc':story_doc,
                    'order_design':order_design,
                    'design_doc':design_doc,
                    'order_color':order_color,
                    'order_fabric':order_fabric,
                    'fabric_doc':fabric_doc,
                    'order_qty':order_qty,
                    'order_status':'pending',
                    'order_date': time.strftime("%d/%m/%Y"),
                    'order_time': time.strftime("%H:%M:%S")
                }

                col_user_orders.insert_one(order)

                o_email = col_user_credentials.find_one({'username':order_username})['email']
                sender = 'badricloudaws@gmail.com'
                receivers = [o_email]
                
                msg = email.mime.Multipart.MIMEMultipart()
                msg['Subject'] = 'Your Order Has Been Confirmed'
                msg['From'] = 'badricloudaws@gmail.com'
                msg['To'] = o_email

                # The main body is just another attachment
                if order_qty == 'Swatch':
                    price = int(fabric_doc['fabric_price']['swatch'])
                else:
                    order_qty = int(order_qty)
                    if 5 <= order_qty <20:
                        price = order_qty*int(fabric_doc['fabric_price']['5-20'])
                    elif 20 <= order_qty <50:
                        price = order_qty*int(fabric_doc['fabric_price']['20-50'])
                    elif 50 <= order_qty <100:
                        price = order_qty*int(fabric_doc['fabric_price']['50-100'])
                    elif order_qty >= 100:
                        price = order_qty*int(fabric_doc['fabric_price']['100-'])
                price = str(price)
                order_qty = str(order_qty)                
                text = 'Your order of ' + design_doc['design_uid'] + (item for item in design_doc['colors'] if item["color_image"] == order_color).next()['color_name'] + ' of length '+ order_qty + ' m has been confirmed. The total amount payable is Rs.'+price
                body = email.mime.Text.MIMEText(text, 'plain')
                msg.attach(body)                
                
                s = smtplib.SMTP('in-v3.mailjet.com', 587)
                s.starttls()
                s.login('a2f9101587dc0b7aec3509c0a35b99d8', 'd4d1385b7e7b2892074f49731ed192a5')
                s.sendmail(sender, receivers, msg.as_string())
                s.quit()
                return 'true'            

@app.route('/ordershistory/<username>')
def ordershistory(username):
    if ('username' in session) and ('password' in session):
        user_credential = check_auth(session['username'],session['password'])        
        if user_credential:
            if session['username'] == username:
                all_orders = []
                for order_doc in col_user_orders.find({'order_username':username}):                    
                    order_doc['color_doc'] = (item for item in order_doc['design_doc']['colors'] if item["color_image"] == order_doc['order_color']).next()
                    _id = order_doc['_id']
                    order_doc['string_id'] = str(_id)

                    all_orders.append(order_doc)
                all_orders.reverse()

                return render_template('display_orders.html', data={'all_orders':all_orders, 'user_doc':user_credential})

app.secret_key = '\xed\xceG\x9a\xd4\x98s0JE\x8c/J2Oj=-!v\xe4\xfb\xf4T'