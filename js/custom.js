
$(function(){

  $('#placeOrderBtn').on('click', function(){
    $('#confirmOrder').modal('show');
  });


  $('#orderSwatchBtn').on('click', function(){
    $('#confirm-swatch').modal('show');
  });

  //landing page carousel
  $('#ds-slider .owl-carousel').owlCarousel({
    autoplay: false,
    autoplayHoverPause: false,
    autoplayTimeout: 5000,
    loop:true,
    margin:10,
    dots: true,
    nav:false,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:1
      },
      1000:{
        items:1
      }
    },
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
  });


  //hover effect for story page
  $('.img-wrapper').hover(function(e){

    if(e.type === 'mouseenter'){
      $(this).find('.view-details-wrapper').fadeIn();      
      $(this).find('.view-design-details').fadeIn();


      $(this).find('img').css({
        "transform" : "scale(1.2,1.2)",
        "transition" : "0.4s ease-out"
      })

    }else{
      $(this).find('.view-details-wrapper').fadeOut();
      $(this).find('.view-design-details').fadeOut();
      $(this).find('img').css({
        "transform" : "scale(1,1)",
        "transition" : "0.4s ease-out"
      })
    }

  });

});